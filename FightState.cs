﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    interface IFightState
    {
        bool IsInHerosRange();
        bool IsInBotsRange();
    }
}
