﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp7
{
    public class FightNavigator : DbContext, IFightState, IParameters, FightLevelCalculator
    {
        Random rnd = new Random();
        Character[] loc = DbContext.chars;
        public string visual;
        public bool heroislive = true;
        public bool atleastalive = true;
        int lvl = 1;
        Dictionary<int, int> HealtBase = new Dictionary<int, int>();
        Dictionary<int, int> Strengthbase = new Dictionary<int, int>();

        public int HeartLoc { get; set; }
        public int StrenghtLoc { get; set; }

        public bool IsInBotsRange()
        {
            if (loc[0].CurrLoc + loc[lvl].Range > loc[lvl].CurrLoc)
            {
                Console.WriteLine(DbContext.chars[0].Currhealth);
                Console.WriteLine(DbContext.chars[lvl].Currhealth);
                Console.WriteLine(lvl);
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsInHerosRange()
        {
            if (loc[0].CurrLoc + loc[0].Range >= loc[lvl].CurrLoc)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public void move()
        {
            if (atleastalive)
            {
                if (lvl >= loc.Length - 1)
                    atleastalive = false;
                    
                if (heroislive)
                {
                    Life();
                    Strenght();

                    Console.WriteLine("funcqia gamodzaxebulia");
                    if (IsInHerosRange())
                    {
                        loc[lvl].Currhealth -= loc[0].DmgPerAttack;
                        Console.WriteLine($"gmiris sheteva {loc[1].DmgPerAttack}");
                        Console.WriteLine($"botis sicocxle{loc[lvl].Currhealth}");
                        if (loc[lvl].Currhealth < 0)
                            lvl++;
                    }
                    else
                    {
                        loc[0].CurrLoc += 10;
                    }

                    Console.WriteLine($"gmiris lokacia {loc[0].CurrLoc}");

                    if (loc[lvl].Currhealth <= 0)
                    {
                        lvl++;
                    }



                    if (IsInBotsRange())
                    {
                        loc[0].Currhealth -= loc[lvl].DmgPerAttack;
                        loc[0].CurrLoc += 5;
                        
                    }
                    if (loc[0].Currhealth <= 0)
                    {
                        this.heroislive = false;
                    }

                    if (HealtBase.ContainsKey(loc[0].CurrLoc))
                    {
                        loc[0].Currhealth += HealtBase[loc[0].CurrLoc];
                    }

                    if (Strengthbase.ContainsKey(loc[0].CurrLoc))
                    {
                        loc[0].DmgPerAttack += Strengthbase[loc[0].CurrLoc];
                    }
                    if ((lvl == loc.Length - 1) && loc[lvl].Currhealth <= 0)
                    {
                        this.atleastalive = false;
                    }
                }
                else
                {
                    Console.WriteLine("gmiri mokvda");
                }

            }
            else
            {
                Console.WriteLine("gmirma moigo");
            }
        }

        public void Life()
        {
            int prob = rnd.Next(0, 30);
            int lifeloc = rnd.Next(1, 200);

            if (prob > 5 && !HealtBase.ContainsKey(lifeloc))
            {
                HealtBase.Add(lifeloc, rnd.Next(1, 50));
            }
        }

        public void Strenght()
        {
            int prob = rnd.Next(0, 20);
            int strenghloc = rnd.Next(1, 200);

            if (prob > 5 && !Strengthbase.ContainsKey(strenghloc))
            {
                Strengthbase.Add(strenghloc, rnd.Next(1, 5));
            }
        }

        public void level()
        {
            for (int i = 1; i < loc.Length - 1; i++)
            {
                if (loc[i].Currhealth > 0)
                {
                    lvl = i;
                }
            }
        }

        public override string ToString()
        {
            visual = string.Empty;
            int z = 0;
            while (loc[0].CurrLoc > z && heroislive && atleastalive)
            {
                z+=10;
                visual = visual + '_';
            }
            visual = visual + "HeroLocation";
            return visual;
        }
    }
}
