﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    abstract class BaseClass 
    {
        public string TypesOfChars { get; set; }
        public string TypeOfWeapon { get; set; }
        public int DmgPerAttack { get; set; }
        public int Range { get; set; }

        


        Random rand = new Random();

        private int _maxlife;
        public  int MaxLife
        {
            get { return _maxlife; } 
            set
            {
                if (value > 100000) 
                { throw new ArgumentException("სიცოცხლე 100-ზე მეტი ვერ იქნება."); }
                else 
                { _maxlife = value; }
            }
        }

        public BaseClass(int mx, string type, int dmg, int range)
        {
            this.MaxLife = mx;
            this.TypesOfChars = type;
            this.DmgPerAttack = dmg;
            this.Range = range;
        }
    }
}
