﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    class Character : BaseClass
    {
        Random rnd = new Random();
        public string Name { get; set; }
        public int Currhealth { get; set; }
        public int CurrLoc { get; set; }

        public Character(int maxlife, string name, string typo, int dmgg, int range, int charloc)
            : base(maxlife, typo, dmgg, range)
        {
            this.Name = name;
            this.Currhealth = maxlife; //rnd.Next(1, 200);
            this.CurrLoc = charloc;
        }

    }
}
